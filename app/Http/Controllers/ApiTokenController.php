<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ApiTokenController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function login(Request $request)
    {
    
        $url = 'http://localhost:8000/oauth/token';
        $http = new \GuzzleHttp\Client;

        try {
            $response = $http->post('http://localhost/mueblestodococina/public/oauth/token', [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id'     => 4,
                    'client_secret' => '79DwXgGaMRB2w1OgfG7mwae77FZQvw17mPtyxrLc',
                    'code' => $request->code,
                    'username' => $request->username,
                    'password' => $request->password,
                ],
            ]);
            $data = json_decode($response->getBody());
            $user = \App\User::select('id','email','name')->where('email',$request->username)->first();  
            $data->user = $user->toArray();
            //dd($data);

            return response()->json($data);

        } catch (\Throwable $th) {
            return response()->json(false);
        }

        $data->user = auth()->user();
        
        return response()->json($data);
       
    }
    
    public function logout()
    {
        auth()->user()->tokens->each(function ($token, $key) {
            $token->delete();
        });
        
        return response()->json('Logged out successfully', 200);
    }





}
