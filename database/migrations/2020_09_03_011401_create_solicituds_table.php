<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSolicitudsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicituds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('local_id')->unsigned();
            $table->bigInteger('sucursal_id')->unsigned();
            $table->bigInteger('agenda_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('estado_solicitud_id')->unsigned();
            $table->time('hora_inicio');
            $table->time('hora_fin');
            $table->timestamps();
            $table->foreign('local_id')->references('id')->on('locals')->constrained();
            $table->foreign('sucursal_id')->references('id')->on('sucursals')->constrained();
            $table->foreign('agenda_id')->references('id')->on('agendas')->constrained();
            $table->foreign('user_id')->references('id')->on('users')->constrained();
            $table->foreign('estado_solicitud_id')->references('id')->on('estado_solicituds')->constrained();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicituds');
    }
}
