<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgendaProductoServiciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agenda_producto_servicios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('agenda_id')->unsigned();
            $table->bigInteger('producto_servicio_id')->unsigned();
            $table->bigInteger('cantidad');
            $table->timestamps();
            $table->foreign('agenda_id')->references('id')->on('agendas')->constrained();
            $table->foreign('producto_servicio_id')->references('id')->on('producto_servicios')->constrained();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agenda_producto_servicios');
    }
}
