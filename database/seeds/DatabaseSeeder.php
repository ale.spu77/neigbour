<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);


        $user = [
    		[
    			'name' => 'Francisco Ibaceta',
    			'email' => 'fibaceta.arce@gmail.com',
    			'email_verified_at' => now(),
    			'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
    			'remember_token' => Str::random(10),
    		]
    	];

    	foreach ($user as $Item){
	        DB::table('users')->insert(['name' => $Item["name"], 'email' => $Item["email"], 'email_verified_at' => $Item["email_verified_at"], 'password' => $Item["password"], 'remember_token' => $Item["remember_token"]]);
	    }


	    $local = [
	    	[
	    		'nombre' => 'LOCAL EJEMPLO 1',
	    		'activado' => true
	    	],//local 1
	    	[
	    		'nombre' => 'LOCAL EJEMPLO 2',
	    		'activado' => true
	    	],//local 2
	    	[
	    		'nombre' => 'LOCAL EJEMPLO 3',
	    		'activado' => true
	    	],//local 3
	    ];

	    foreach ($local as $Item){
	        DB::table('locals')->insert(['nombre' => $Item["nombre"], 'activado' => $Item["activado"]]);
	    }

	    $sucursal = [
	    	[
	    		'local_id' => '1',
	    		'nombre' => 'Sucursal EJEMPLO 1',
	    		'direccion' => 'direccion 1',
	    		'activado' => true
	    	],//sucursal 1
	    	[
	    		'local_id' => '2',
	    		'nombre' => 'Sucursal EJEMPLO 2',
	    		'direccion' => 'direccion 2',
	    		'activado' => true
	    	],//sucursal 1
	    	[
	    		'local_id' => '1',
	    		'nombre' => 'Sucursal EJEMPLO 3',
	    		'direccion' => 'direccion 3',
	    		'activado' => true
	    	],//sucursal 1
	    ];

	    foreach ($sucursal as $Item){
	        DB::table('sucursals')->insert([
	        	'local_id' => $Item["local_id"], 
				'nombre' => $Item["nombre"], 
				'direccion' => $Item["direccion"],
				'activado' => $Item["activado"]
			]);
	    }


	    $usuario_local_sucursal = [
	    	[
	    		'user_id' => '1',
	    		'local_id' => '1',
	    		'sucursal_id' => '1'
	    	],//local 1
	    	[
	    		'user_id' => '1',
	    		'local_id' => '2',
	    		'sucursal_id' => '2'
	    	],//local 2
	    	[
	    		'user_id' => '1',
	    		'local_id' => '3',
	    		'sucursal_id' => '3'
	    	],//local 3
	    ];

	    foreach ($usuario_local_sucursal as $Item){
	        DB::table('usuario_local_sucursals')->insert(['user_id' => $Item["user_id"], 'local_id' => $Item["local_id"], 'sucursal_id' => $Item["sucursal_id"]]);
	    }


	    $tipo_producto_servicio = [
	    	[
	    		'nombre' => 'servicio'
	    	],
	    	[
	    		'nombre' => 'producto'
	    	]
	    ];

	    foreach ($tipo_producto_servicio as $Item){
	        DB::table('tipo_producto_servicios')->insert(['nombre' => $Item["nombre"]]);
	    }


	    $producto_servicio = [
	    	[
	    		'tipo_producto_servicio_id' => '1',
	    		'nombre' => 'corte ejemplo 1',
	    		'descripcion' => 'corte bla bla bla',
	    		'valor' => '6000',
	    		'duracion' => '60',
	    		'imagen' => '',
	    	],
	    	[
	    		'tipo_producto_servicio_id' => '1',
	    		'nombre' => 'corte ejemplo 2',
	    		'descripcion' => 'corte bla bla bla',
	    		'valor' => '6500',
	    		'duracion' => '60',
	    		'imagen' => '',
	    	],
	    	[
	    		'tipo_producto_servicio_id' => '2',
	    		'nombre' => 'crema',
	    		'descripcion' => 'crema bla bla bla',
	    		'valor' => '10000',
	    		'duracion' => '',
	    		'imagen' => '',
	    	],
	    ];

	    foreach ($producto_servicio as $Item){
	        DB::table('producto_servicios')->insert([
	        	'tipo_producto_servicio_id' => $Item["tipo_producto_servicio_id"], 
				'nombre' => $Item["nombre"], 
				'descripcion' => $Item["descripcion"],
				'valor' => $Item["valor"],
				'duracion' => $Item["duracion"],
				'imagen' => $Item["imagen"],
			]);
	    }







    }
}
