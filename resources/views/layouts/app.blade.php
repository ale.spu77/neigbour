<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <div class="container">    
            <nav class="navbar navbar-dark bg-dark shadow-xl navbar-expand-lg ">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                    <a class="navbar-brand" href="#">Neighbour</a>
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                        <!-- Authentication Links -->
                                @guest
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                    </li>
                                    @if (Route::has('register'))
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                        </li>
                                    @endif
                                @else
                                <li class="nav-item active">
                                    <a class="nav-link" href="#">Inicio <span class="sr-only">(current)</span></a>
                                </li>

                                <li class="nav-item active">
                                    <a class="nav-link" href="#">Calendario <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="nav-item active">
                                    <a class="nav-link" href="#">Perfil <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="nav-item active">
                                    <a class="nav-link" href="#">Productos/Servicios<span class="sr-only">(current)</span></a>
                                </li>
                                <li class="nav-item active">
                                    <a class="nav-link" href="#">Empresa <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        Mantenedor
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="">
                                            Usuarios
                                        </a>
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="">
                                            Permisos
                                        </a>
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="">
                                            Productos
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                            @csrf
                                        </form>
                                    </div>
                                </li>

                                
                                <form class="form-inline my-2 my-lg-0">
                                    <input class="form-control mr-sm-2" type="search" placeholder="" aria-label="Search">
                                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
                                </form>
                                
                                </ul>
                                <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {{ Auth::user()->name }}
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                                @endguest
                </div>
            </nav>
        </div>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>

<style type="text/css">
    .card{
        margin-top: 110px;
    }
    #app .navbar{

        min-height: 100px;
        box-shadow: 1px 1px 6px 1px #000;
        position: fixed;
        z-index: 2;
         width: 76%;
         opacity: 85%;
         border-radius: 5px;
         margin-top: 20px;
        height: 50px;
       /*  background-color: aqua !important;*/
       background: rgba(33,33,33,1);
background: -moz-linear-gradient(-45deg, rgba(33,33,33,1) 0%, rgba(13,13,13,1) 21%, rgba(46,46,46,1) 38%, rgba(31,31,31,1) 48%, rgba(0,0,0,1) 63%, rgba(20,20,20,1) 72%, rgba(8,8,8,1) 80%, rgba(12,12,12,1) 87%, rgba(13,13,13,1) 89%, rgba(13,13,13,1) 100%);
background: -webkit-gradient(left top, right bottom, color-stop(0%, rgba(33,33,33,1)), color-stop(21%, rgba(13,13,13,1)), color-stop(38%, rgba(46,46,46,1)), color-stop(48%, rgba(31,31,31,1)), color-stop(63%, rgba(0,0,0,1)), color-stop(72%, rgba(20,20,20,1)), color-stop(80%, rgba(8,8,8,1)), color-stop(87%, rgba(12,12,12,1)), color-stop(89%, rgba(13,13,13,1)), color-stop(100%, rgba(13,13,13,1)));
background: -webkit-linear-gradient(-45deg, rgba(33,33,33,1) 0%, rgba(13,13,13,1) 21%, rgba(46,46,46,1) 38%, rgba(31,31,31,1) 48%, rgba(0,0,0,1) 63%, rgba(20,20,20,1) 72%, rgba(8,8,8,1) 80%, rgba(12,12,12,1) 87%, rgba(13,13,13,1) 89%, rgba(13,13,13,1) 100%);
background: -o-linear-gradient(-45deg, rgba(33,33,33,1) 0%, rgba(13,13,13,1) 21%, rgba(46,46,46,1) 38%, rgba(31,31,31,1) 48%, rgba(0,0,0,1) 63%, rgba(20,20,20,1) 72%, rgba(8,8,8,1) 80%, rgba(12,12,12,1) 87%, rgba(13,13,13,1) 89%, rgba(13,13,13,1) 100%);
background: -ms-linear-gradient(-45deg, rgba(33,33,33,1) 0%, rgba(13,13,13,1) 21%, rgba(46,46,46,1) 38%, rgba(31,31,31,1) 48%, rgba(0,0,0,1) 63%, rgba(20,20,20,1) 72%, rgba(8,8,8,1) 80%, rgba(12,12,12,1) 87%, rgba(13,13,13,1) 89%, rgba(13,13,13,1) 100%);
background: linear-gradient(135deg, rgba(33,33,33,1) 0%, rgba(13,13,13,1) 21%, rgba(46,46,46,1) 38%, rgba(31,31,31,1) 48%, rgba(0,0,0,1) 63%, rgba(20,20,20,1) 72%, rgba(8,8,8,1) 80%, rgba(12,12,12,1) 87%, rgba(13,13,13,1) 89%, rgba(13,13,13,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#212121', endColorstr='#0d0d0d', GradientType=1 );
    } 

</style>
