import App from './components/App.vue';
import UploadComponent from './components/UploadImage.vue';
import LoginComponent from './components/login.vue';
import InicioComponent from './components/inicio.vue';

const routes = [
    {
        path: '/',
        component: InicioComponent,
    },

    {
        path: '/login/api',
        component: LoginComponent,
    },
    {
        path: '/image',
        component: UploadComponent,
        children: [
            {
                path: 'get',
            },
        ]
    },
]

export default routes