require('./bootstrap');
require('./bootstrap');
window.Vue = require('vue');
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import('vue-it-bigger/dist/vue-it-bigger.min.css') // when using webpack
import routes      from './routes'
import VueRouter   from 'vue-router'
import VueTouch    from 'vue-touch'
import VueLazyLoad from 'vue-lazyload'

//Componentes
import App      from './components/App.vue'
import Galeria  from './components/galeria.vue'
import NavbarUp from './components/Navbar-up.vue'
import LightBox from 'vue-it-bigger'
import vGallery from 'v-gallery'

Vue.use(vGallery)
Vue.use(VueLazyLoad)
Vue.use(VueTouch, { name: 'v-touch' })

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(VueRouter)
Vue.component("galeria", Galeria)
Vue.component("app", App)
Vue.component("navbar-up", NavbarUp)
Vue.component('LightBox', LightBox);
Vue.component('login',  require('./components/login.vue').default);


import passportclients    from './components/passport/Clients.vue';
import passportauthorized from './components/passport/AuthorizedClients.vue';
import passportpersonal   from './components/passport/PersonalAccessTokens.vue';

Vue.component(
    'passport-clients',
    passportclients
);
Vue.component(
    'passport-authorized-clients',
    passportauthorized
);
Vue.component(
    'passport-personal-access-tokens',
    passportpersonal
);



Object.defineProperty(Vue.prototype, '$swal', {
  get() {
    return require('sweetalert2')
  }
})
const router = new VueRouter({
  // mode: 'history',
  routes,
});

axios.interceptors.request.use(function (config) {
  let token = localStorage.getItem('access_token')
  if (token) {
     config.headers['Authorization'] = `Bearer ${token}`
  }
  return config;
}, function (error) {
  return Promise.reject(error);
});

Vue.prototype.$store = {
  token:'aa2',
  set(data){
    this.token=data;
  }
};


new Vue({
  router,
  data:{
    session:{}
  },
  render: h => h(App), // mount the base component
 
}).$mount('#app')
