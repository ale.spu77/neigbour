<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/token', 'ApiTokenController@update')->name('api.token');
Route::get('/image/get', 'ImageController@index');
Route::get('/get/media', 'ImageController@getMedia');

Route::post('/get/categoria', 'ImageController@getCategoria');
Route::post('/image/upload', 'ImageController@store');
Route::POST('/session', 'HomeController@session')->name('session');
Route::POST('/contacto', 'HomeController@contactoSave')->name('api.contacto');

Route::post('/login', 'ApiTokenController@login');

